# App of Apps avec ArgoCD

Ce projet utilise le pattern **App of Apps** d'ArgoCD pour gérer et déployer des applications Kubernetes. Le concept **App of Apps** permet de définir une application "root" dans ArgoCD, qui à son tour déploie et gère plusieurs autres applications à partir d'un dépôt Git. Ce modèle est particulièrement utile pour orchestrer de multiples applications en facilitant leur gestion à grande échelle.

## Structure du dépôt

Voici la structure du dépôt utilisée pour ce projet :

```
.
├── apps
│   ├── app1
│   │   └── spec.yaml          # Spécification Kustomize de app1
│   ├── app2
│   │   └── spec.yaml          # Spécification Kustomize de app2
│   ├── appN
│   │   └── spec.yaml          # Spécification Kustomize de appN
│   ├── app1.yaml              # Fichier ArgoCD pour déployer app1
│   ├── app2.yaml              # Fichier ArgoCD pour déployer app2
│   └── appN.yaml              # Fichier ArgoCD pour déployer appN
└── root-app.yaml               # Fichier root app ArgoCD
```

- **`root-app.yaml`** : Ce fichier est l'application "root" qui déploie toutes les autres applications définies dans le répertoire `apps/`.
- **`apps/`** : Ce répertoire contient les différentes applications (app1, app2, appN) qui sont déployées par l'application "root".

## Déploiement de l'application "root"

### Pré-requis

- ArgoCD doit être installé et configuré dans votre cluster Kubernetes.
- `kubectl` doit être configuré pour pointer sur le cluster où ArgoCD est installé.

### 1. Déployer l'application root

La première étape consiste à déployer l'application **root-app** qui va orchestrer le déploiement des autres applications. Ce déploiement se fait en appliquant directement le fichier de l'application root dans le cluster Kubernetes avec la commande `kubectl`.

Exécutez la commande suivante **uniquement la première fois** pour créer l'application root dans ArgoCD :

```bash
kubectl apply -f root-app.yaml
```

### 2. Synchronisation automatique avec Git

Une fois l'application **root-app** déployée, ArgoCD prendra en charge automatiquement la synchronisation et le déploiement des autres applications. Grâce à la configuration d'auto-sync dans le fichier `root-app.yaml`, ArgoCD :
- Surveillera continuellement les modifications dans le dépôt Git.
- Synchronisera automatiquement les applications lorsque des changements seront détectés.
- Supprimera les ressources qui ne sont plus présentes dans le dépôt Git.
- Réparera les divergences entre l'état réel du cluster et la configuration définie dans Git.

### 3. Gestion des applications

Les applications enfants (app1, app2, etc.) sont définies dans le répertoire `apps/`. Chaque application est gérée de manière indépendante à travers ArgoCD, mais orchestrée par l'application "root-app". Vous pouvez ajouter, mettre à jour ou supprimer des applications dans le répertoire `apps/` et ArgoCD synchronisera automatiquement ces changements.

.